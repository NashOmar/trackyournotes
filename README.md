Track Your Notes is a Flask-powered web application that allows users to create, view, and manage notes.

## Installation

Follow these steps to run this application locally:

1. **Cloning the repository** .

   - Open the terminal or command prompt.

   - Clone the GitLab repository to your local machine:

     ```shell
     git clone https://gitlab.com/NashOmar/trackyourscript.git
     ```

2. **Create a Virtual Environment** .

   - Go to the project folder:

     ```shell
     cd track your notes
     ```

   - Create a virtual environment to reduce dependents:

     ```shell
     Python -m venv pretty solid
     ```

   - Enable virtual environment:
     - On Windows:
       ```shell
       venv\Scripts\Disable
       ```
     - On macOS and Linux:
       ```shell
       Source: Venv/Bin/Active
       ```

3. **Install Dependencies** .

   - Install the required Python package:

     ```shell
     pip install -r required.txt
     ```

4. **configure environmental variables** .
Create an `.env` file in the project root and add the following line, replacing it with the appropriate production database URL:

     ```
     DATABASE_URL=sqlite:///comments.db
     ```

5. **Start the database** .

   - Open a Python container:
     ```shell
     Working with Python
     ```

   - In the Python shell, initialize the database:
     ```Processing Python
     from app import db
     with app.app_context();
         db.create_all() .
     ```

6. **Submit changes to Git** .

   - Commit your changes by adding all project files to the Git repository:
     ```shell
     git in it
     git commit -m "Initial commit".
     git push initial master
     ```

7. **Install GitLab CI/CD** .

   - Installed GitLab CI/CD to enable deployment and testing. Manual steps are not required.

8. **Note GitLab environment changes** .

   - In the GitLab project settings, go to "Settings" > "CI/CD" > "variables" and add the following variables:
     - `Function` with value `FLASK_ENV`
     - `app.py` with value `FLASK_APP`
     - `SECRET_KEY` a secure secret key for Flask processing

9. **Programmers and Analysts** .

   - GitLab CI/CD will run the application on our runner when changes are pushed to the `master` branch. Check pipeline status in GitLab for successful deployments.

## Survey

1. **To access the app:**

   - The application is available through the hosting at http://127.0.0.1:5000/

2. **creating notes:**

   - Click the "Add Note" button to create a new note.
   - Give your essay a title and description.

3. **Edit Note:**

   - Click the "Edit" button next to the note to make changes.
   - Change the title and description if necessary.

4. **deleting notes:**

   - Click the "Delete" button to remove the comment.
   - Confirm the action when promted.
Contributions to this project are welcome! To contribute:

1. Fork the repository.
2. Create a branch for your feature or bug fix.
3. Make your changes and commit them.
4. Create a pull request with a clear description of your changes.

## License

This project is licensed under the MIT License. See the [LICENSE.md](LICENSE.md) file for details.
